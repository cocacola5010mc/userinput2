import re
import json
import os

localItemState = []
isLookingForInputs = True 


# definitions

def generateDoWhile (testFn, valueFn):
    def fn ():
        value = valueFn()
        if testFn(value):
            return value
        else:
            return fn ()
    return fn

def isNotEmptyString (string):
    return (string != '')

def isNotNone (string):
    return (string != 'None')

def isPoundFormat (string):
    return re.match('£[0-9]+\.[0-9]{2}', string) # The match object that match may return has boolean value of true

def isNotQuit (string):
    if string == 'quit':
        print('Quitting')
        exit(0)
    else:
        return True 

def wrappedInput (message):
    def fn ():
        return input(message)
    return fn

def quitableFilledInput (message): # Somewhat of just an alias for these basic tests I want to run
    return generateDoWhile(isNotEmptyString, generateDoWhile(isNotQuit, wrappedInput(message)))()

def wrappedQuittableFilledInput (message):
    def fn ():
        return quitableFilledInput(message)
    return fn

def createItemStorage ():
    f = open('data.json', 'w')
    f.write('[]')
    f.close()

def updateLocalState ():
    global localItemState
    f = open('data.json', 'r')
    localItemState = json.loads(f.read())
    f.close()

def saveLocalState ():
    f = open('data.json', 'w')
    f.write(json.dumps(localItemState))
    f.close()

def findItemsAdveragePrice ():
    priceSum = 0
    for item in localItemState:
        priceSum += float(item['price'])
    if len(localItemState) == 0:
        return 0
    else:
        return priceSum / len(localItemState)

def findItemTotalSum ():
    priceSum = 0
    for item in localItemState:
        if item['price'] > 50:
            priceSum += float(item['price']) * 0.95
        else:
            priceSum += float(item['price'])
    return priceSum * 1.2

def parsePounds (string):
    return float(string[1:])


# Code

if not os.path.exists('data.json'):
    createItemStorage()
    print('Making item storage')

updateLocalState()

while isLookingForInputs:
    saveLocalState()
    
    productName = quitableFilledInput('Enter your product name or "None" to stop: ')
    if productName == 'None':
        isLookingForInputs = False
    else:
        productPrice = parsePounds(generateDoWhile(isPoundFormat, wrappedQuittableFilledInput('Enter the price (£x.xx): '))())
        localItemState.append({"name": productName,"price": productPrice})

saveLocalState()
localItemState.sort(key = lambda i: float(i['price']))

print('The most expensive item is: ' + localItemState[0]['name'])
print('The least expensive item is: ' + localItemState[len(localItemState) - 1]['name'])
print('The adverage item price is: £' + str(round(findItemsAdveragePrice(), 2)))
print('The total cost of items with VAT and item discounts is: £' + str(round(findItemTotalSum(), 2)))
